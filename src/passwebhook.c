#include <portable.h>

#ifndef SLAPD_OVER_PASSWEBHOOK
#define SLAPD_OVER_PASSWEBHOOK SLAPD_MOD_DYNAMIC
#endif

#ifdef SLAPD_OVER_PASSWEBHOOK

#include <slap.h>
#include <ac/errno.h>
#include <ac/string.h>

#include "config.h"

#include <time.h>
#include <stdio.h>
#include <string.h>
#include <curl/curl.h>
#include <json.h>
#include <openssl/sha.h>
#include <openssl/hmac.h>

#define NOTIFY_SUCCESS 0
#define NOTIFY_MISSING_CONF 1
#define NOTIFY_INTERNAL_ERROR 2
#define NOTIFY_WEBHOOK_ERROR 3

#define AUTH_HEADER "X-Token: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
#define HMAC_SIZE SHA_DIGEST_LENGTH

/* Per-instance configuration information */
typedef struct passwebhook_t {
	char *webhook_uri;
	char *webhook_secret;
} passwebhook_t;

typedef struct passwebhook_entry_t {
	passwebhook_t *pi;
	struct berval *dn;
	struct berval *password;
} passwebhook_entry_t;

void fill_json_obj(struct json_object *jobj, Operation *op, const char *dn, const char *password,
		const char *action) {
	json_object_object_add(jobj, "timestamp", json_object_new_int64(time(NULL)));
	json_object_object_add(jobj, "conn_id",   json_object_new_int64(op->o_connid));
	json_object_object_add(jobj, "op_id",     json_object_new_int64(op->o_opid));
	json_object_object_add(jobj, "requestor", BER_BVISNULL(&op->o_dn) ? NULL : json_object_new_string(op->o_dn.bv_val));
	json_object_object_add(jobj, "dn",        dn ? json_object_new_string(dn) : NULL);
	json_object_object_add(jobj, "password",  password ? json_object_new_string(password) : NULL);
	json_object_object_add(jobj, "action",    json_object_new_string(action));
}

static void curl_destroy(void *key, void *data) {
	Debug(LDAP_DEBUG_ANY, "passwebhook: cURL key the destroy: %ld\n", data, NULL, NULL);
	if (data) {
		curl_easy_cleanup(data);
	}
}

static int notify_webhook(Operation *op, passwebhook_t *pi, const char *dn, const char *password,
		const char *action, const char **err_text) {
	int ret = NOTIFY_SUCCESS;
	CURL *curl = NULL;
	CURLcode res;

	struct curl_slist *headers = NULL;
	struct json_object *jobj;
	const char *json_content;
	unsigned char* json_digest;
	char x_token_header[] = AUTH_HEADER;

	/* Debug(LDAP_DEBUG_ANY, "passwebhook: webhook=\"%s\"\n", pi->webhook_uri, NULL, NULL);
	Debug(LDAP_DEBUG_ANY, "passwebhook: dn=\"%s\"\n", dn, NULL, NULL); */

	if (pi->webhook_secret == NULL) {
		Debug(LDAP_DEBUG_ANY, "passwebhook: webhook secret MUST be set\n", NULL, NULL, NULL);

		if (err_text)
			*err_text = "Webhook error. The secret is not set.";

		return NOTIFY_MISSING_CONF;
	}

	if (ldap_pvt_thread_pool_getkey(op->o_threadctx, notify_webhook, &curl, NULL) == ENOENT || curl == NULL) {
		curl = curl_easy_init();

		if (curl == NULL)
			return NOTIFY_INTERNAL_ERROR;

		ldap_pvt_thread_pool_setkey(op->o_threadctx, notify_webhook, curl, curl_destroy, NULL, NULL);
	} else {
		curl_easy_reset(curl);
	}

	jobj = json_object_new_object();
	fill_json_obj(jobj, op, dn, password, action);
	json_content = json_object_to_json_string_ext(jobj, JSON_C_TO_STRING_PLAIN);

	json_digest = HMAC(EVP_sha1(), pi->webhook_secret, strlen(pi->webhook_secret), (unsigned char*)json_content, strlen(json_content), NULL, NULL);
	for (int i = 0; i < HMAC_SIZE; i++) {
		sprintf(x_token_header + sizeof(x_token_header) - 1 - HMAC_SIZE*2 + i*2, "%02x", (unsigned int)json_digest[i]);
	}

	curl_easy_setopt(curl, CURLOPT_URL, pi->webhook_uri);
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 3L);

	headers = curl_slist_append(headers, "Content-Type: application/json");
	headers = curl_slist_append(headers, x_token_header);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, json_content);

	Debug(LDAP_DEBUG_ANY, "passwebhook: Notifying webhook of %s on %s\n", action, dn, NULL);
	res = curl_easy_perform(curl);
	if (res != CURLE_OK) {
		const char *curl_err;
		curl_err = curl_easy_strerror(res);

		Debug(LDAP_DEBUG_ANY, "passwebhook: Error on webhook call: %s\n", curl_err, NULL, NULL);

		ret = NOTIFY_INTERNAL_ERROR;

		if (err_text)
			*err_text = curl_err;

	} else {
		long response_code;
		char webhook_error_str[64];

		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);

		sprintf(webhook_error_str, "The webhook answered with HTTP/%ld", response_code);
		Debug(LDAP_DEBUG_ANY, "passwebhook: %s\n", webhook_error_str, NULL, NULL);

		if (response_code < 200 || response_code > 299) {
			ret = NOTIFY_WEBHOOK_ERROR;
			if (err_text)
				*err_text = webhook_error_str;
		}
	}

	json_object_put(jobj); // Delete the json object
	curl_slist_free_all(headers);

	return ret;
}

static int passwebhook_modules_init(passwebhook_t *pi);

static int passwebhook_op_cleanup(Operation *op, SlapReply *rs) {
	slap_callback *cb;

	/* Clear out the current key */
	ldap_pvt_thread_pool_setkey(op->o_threadctx, passwebhook_op_cleanup, NULL, 0, NULL, NULL);

	/* Free the callback */
	cb = op->o_callback;
	op->o_callback = cb->sc_next;

	if (cb->sc_private) {
		passwebhook_entry_t *pchr = cb->sc_private;

		ber_bvfree_x(pchr->dn, op->o_tmpmemctx);
		ber_bvfree_x(pchr->password, op->o_tmpmemctx);
		ch_free(pchr);
	}
	op->o_tmpfree(cb, op->o_tmpmemctx);

	return 0;
}

static int passwebhook_mod_response(Operation *op, SlapReply *rs) {
	int ret = SLAP_CB_CONTINUE;
	int wh_ret;
	const char *err_text = "An unknown error occurred while notifying webhook";

	slap_callback *cb = op->o_callback;
	passwebhook_entry_t *pchr = cb->sc_private;

	/* If the modification is failed, skip */
	if (rs->sr_err != LDAP_SUCCESS)
		return SLAP_CB_CONTINUE;

	Debug(LDAP_DEBUG_ANY, "passwebhook: The password is good to transmit\n", NULL, NULL, NULL);
	wh_ret = notify_webhook(op, pchr->pi, pchr->dn->bv_val, pchr->password->bv_val, "change", &err_text);

	switch (wh_ret) {
	  case NOTIFY_SUCCESS:
		break;
	  case NOTIFY_MISSING_CONF:
	  case NOTIFY_WEBHOOK_ERROR:
		ret = LDAP_UNWILLING_TO_PERFORM;
		break;
	  case NOTIFY_INTERNAL_ERROR:
		ret = LDAP_UNAVAILABLE;
		break;
	  default:
		ret = LDAP_OTHER;
	}

	if (ret != SLAP_CB_CONTINUE)
		send_ldap_error(op, rs, ret, err_text);

	return ret;
}

static int passwebhook_modify(Operation *op, SlapReply *rs) {
	int rc, ret = SLAP_CB_CONTINUE;
	slap_overinst *on = (slap_overinst *)op->o_bd->bd_info;
	passwebhook_t *pi = on->on_bi.bi_private;

	slap_callback *sc, *cb;
	Entry *e;
	Modifications *ml, *addmod;
	struct berval newpw = BER_BVNULL, *bv;
	int passfound = 0;

	passwebhook_entry_t *pchr;

	/* If this is a replica, assume the master checked everything */
	if (SLAPD_SYNC_IS_SYNCCONN(op->o_connid))
		return SLAP_CB_CONTINUE;

	op->o_bd->bd_info = (BackendInfo *)on->on_info;
	rc = be_entry_get_rw(op, &op->o_req_ndn, NULL, NULL, 0, &e);
	op->o_bd->bd_info = (BackendInfo *)on;

	if (rc != LDAP_SUCCESS) return rc;

	for (sc = op->o_callback; sc; sc = sc->sc_next) {
		if (sc->sc_response == slap_null_cb && sc->sc_private) {
			req_pwdexop_s *qpw = sc->sc_private;
			newpw = qpw->rs_new;
			passfound = 1;
			break;
		}
	}

	for (ml = op->orm_modlist; ml != NULL; ml = ml->sml_next) {
		if (ml->sml_desc == slap_schema.si_ad_userPassword &&
		    (ml->sml_op == LDAP_MOD_ADD || ml->sml_op == LDAP_MOD_REPLACE)
		) {
			addmod = ml;
			passfound = 1;
			break;
		}
	}

	if (passfound) {
		/* Debug(LDAP_DEBUG_ANY, "passwebhook: The request contains a password :3\n", NULL, NULL, NULL); */
		bv = newpw.bv_val ? &newpw : &addmod->sml_values[0];

		pchr = ch_calloc(1, sizeof(passwebhook_entry_t));
		pchr->pi = pi;
		pchr->dn = ber_dupbv_x(NULL, &e->e_name, op->o_tmpmemctx);
		pchr->password = ber_dupbv_x(NULL, bv, op->o_tmpmemctx);

		ldap_pvt_thread_pool_setkey(op->o_threadctx, passwebhook_op_cleanup, op, 0, NULL, NULL);
		cb = op->o_tmpcalloc(1, sizeof(slap_callback), op->o_tmpmemctx);
		cb->sc_response = passwebhook_mod_response;
		cb->sc_private = (void *)pchr;
		cb->sc_cleanup = passwebhook_op_cleanup;

		cb->sc_next = op->o_callback;
		op->o_callback = cb;
	}

	op->o_bd->bd_info = (BackendInfo *)on->on_info;
	be_entry_release_r(op, e);

	return ret;
}

static int passwebhook_bind_response(Operation *op, SlapReply *rs) {
	const char *action = "bind-unknown";
	const char *dn, *password;

	slap_callback *cb = op->o_callback;
	passwebhook_t *pi = cb->sc_private;

	dn = BER_BVISNULL(&op->o_req_ndn) ? NULL : op->o_req_ndn.bv_val;
	password = BER_BVISNULL(&op->orb_cred) ? NULL : op->orb_cred.bv_val;

	if (rs->sr_err == LDAP_SUCCESS) {
		action = "bind-success";

		/* Debug(LDAP_DEBUG_ANY, "passwebhook: Caught the BIND password for %s\n",
				dn ? dn : "<empty>", NULL, NULL); */
	} else {
		action = "bind-fail";

		/* Debug(LDAP_DEBUG_ANY, "passwebhook: Caught a bad password for %s [%s]\n",
				dn ? dn : "<empty>", password ? password : "<empty>", NULL); */
	}

	notify_webhook(op, pi, dn, password, action, NULL);

	/* Continue in all cases */
	return SLAP_CB_CONTINUE;
}

static int passwebhook_bind(Operation *op, SlapReply *rs) {
	slap_callback *cb;
	slap_overinst *on = (slap_overinst *) op->o_bd->bd_info;

	cb = op->o_tmpcalloc(sizeof(slap_callback), 1, op->o_tmpmemctx);

	cb->sc_response = passwebhook_bind_response;
	cb->sc_private = on->on_bi.bi_private;

	overlay_callback_after_backover(op, cb, 1);

	return SLAP_CB_CONTINUE;
}

static slap_overinst passwebhook;

/* back-config stuff */
enum {
	PC_WEBHOOK_URI = 1,
	PC_WEBHOOK_SECRET,
};

static ConfigDriver passwebhook_cf_func;

/*
 * NOTE: uses OID arcs OLcfgCtAt:9476.1 and OLcfgCtOc:9476.1
 */

static ConfigTable passwebhook_cfats[] = {
	{ "passwebhook-webhook-url", "arg",
		2, 0, 0, ARG_MAGIC|ARG_STRING|PC_WEBHOOK_URI, passwebhook_cf_func,
		"( OLcfgCtAt:9476.1.1 NAME 'olcPassWebhookURI' "
		"DESC 'Webhook URI' "
		"EQUALITY caseExactMatch "
		"SYNTAX OMsDirectoryString SINGLE-VALUE )", NULL, NULL },
	{ "passwebhook-webhook-secret", "arg",
		2, 0, 0, ARG_MAGIC|ARG_STRING|PC_WEBHOOK_SECRET, passwebhook_cf_func,
		"( OLcfgCtAt:9476.1.2 NAME 'olcPassWebhookSecret' "
		"DESC 'Webhook secret' "
		"EQUALITY caseExactMatch "
		"SYNTAX OMsDirectoryString SINGLE-VALUE )", NULL, NULL },

	{ NULL, NULL, 0, 0, 0, ARG_IGNORED }
};

static ConfigOCs passwebhook_cfocs[] = {
	{ "( OLcfgCtOc:9476.1.1 "
		"NAME 'olcPassWebhookConfig' "
		"DESC 'passwebhook overlay configuration' "
		"SUP olcOverlayConfig "
		"MAY ( "
			"olcPassWebhookURI "
			"$ olcPassWebhookSecret "
		") )", Cft_Overlay, passwebhook_cfats },

	{ NULL, 0, NULL }
};

static int passwebhook_cf_func(ConfigArgs *c) {
	slap_overinst	*on = (slap_overinst *)c->bi;

	int		rc = 0;
	passwebhook_t	*pi = on->on_bi.bi_private;

	if (c->op == SLAP_CONFIG_EMIT) {
		switch (c->type) {
		case PC_WEBHOOK_URI:
			if (!pi->webhook_uri)
				return 1;
			c->value_string = pi->webhook_uri;
			break;

		case PC_WEBHOOK_SECRET:
			if (!pi->webhook_secret)
				return 1;
			c->value_string = pi->webhook_secret;
			break;

		default:
			assert(0);
			rc = 1;
		}
		return rc;

	} else if (c->op == LDAP_MOD_DELETE) {
		switch (c->type) {
		case PC_WEBHOOK_URI:
			ch_free(pi->webhook_uri);
			pi->webhook_uri = NULL;
			break;

		case PC_WEBHOOK_SECRET:
			ch_free(pi->webhook_secret);
			pi->webhook_secret = NULL;
			break;

		default:
			assert(0);
			rc = 1;
		}
		return rc;
	}

	switch (c->type) {
	case PC_WEBHOOK_URI:
		ch_free(pi->webhook_uri);
		pi->webhook_uri = c->value_string;
		break;

	case PC_WEBHOOK_SECRET:
		ch_free(pi->webhook_secret);
		pi->webhook_secret = c->value_string;
		break;

	default:
		assert(0);
		return 1;
	}
	return rc;
}

static int passwebhook_modules_init(passwebhook_t *pi) {
	curl_global_init(CURL_GLOBAL_ALL);

	return 0;
}

static int passwebhook_db_init(BackendDB *be, ConfigReply *cr) {
	slap_overinst	*on = (slap_overinst *)be->bd_info;
	passwebhook_t	*pi;

	pi = ch_calloc(1, sizeof(passwebhook_t));
	if (pi == NULL) {
		return 1;
	}

	on->on_bi.bi_private = (void *)pi;

	return 0;
}

static int passwebhook_db_open(BackendDB *be, ConfigReply *cr) {
	slap_overinst	*on = (slap_overinst *)be->bd_info;
	passwebhook_t	*pi = (passwebhook_t *)on->on_bi.bi_private;

	int	rc;
	rc = passwebhook_modules_init(pi);
	if (rc) {
		return rc;
	}

	return 0;
}

static int passwebhook_db_destroy(BackendDB *be, ConfigReply *cr) {
	slap_overinst	*on = (slap_overinst *)be->bd_info;
	passwebhook_t	*pi = (passwebhook_t *)on->on_bi.bi_private;

	if (pi) {
		ch_free(pi);
	}

	curl_global_cleanup();

	return 0;
}

int passwebhook_initialize(void) {
	int rc;

	passwebhook.on_bi.bi_type = "passwebhook";

	passwebhook.on_bi.bi_db_init = passwebhook_db_init;
	passwebhook.on_bi.bi_db_open = passwebhook_db_open;
	passwebhook.on_bi.bi_db_destroy = passwebhook_db_destroy;

	passwebhook.on_bi.bi_op_modify = passwebhook_modify;
	passwebhook.on_bi.bi_op_bind = passwebhook_bind;
	passwebhook.on_bi.bi_cf_ocs = passwebhook_cfocs;

	rc = config_register_schema(passwebhook_cfats, passwebhook_cfocs);
	if (rc) {
		return rc;
	}

	return overlay_register(&passwebhook);
}

#if SLAPD_OVER_PASSWEBHOOK == SLAPD_MOD_DYNAMIC
int init_module(int argc, char *argv[]) {
	return passwebhook_initialize();
}
#endif

#endif /* defined(SLAPD_OVER_PASSWEBHOOK) */
